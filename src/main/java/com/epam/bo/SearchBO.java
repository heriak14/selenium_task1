package com.epam.bo;

import com.epam.po.GoogleSearchPage;
import com.epam.po.SearchResultPage;

public class SearchBO {
    private GoogleSearchPage searchPage;

    public SearchBO() {
        searchPage = new GoogleSearchPage();
    }

    public void search(String query) {
        searchPage.setKeysToSearchField(query);
        searchPage.clickSearchButton();
    }

    public void switchToImgTab() {
        SearchResultPage resultPage = new SearchResultPage();
        resultPage.clickImageButton();
    }
}
