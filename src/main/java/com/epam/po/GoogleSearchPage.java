package com.epam.po;

import com.epam.utils.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GoogleSearchPage {
    private WebElement searchField;
    private WebElement searchButton;
    private WebDriver driver;

    public GoogleSearchPage() {
        driver = WebDriverManager.getDriver();
        searchField = driver.findElement(By.name("q"));
        searchButton = driver.findElement(By.name("btnK"));
    }

    public void setKeysToSearchField(String keys) {
        searchField.sendKeys(keys);
    }

    public void clickSearchButton() {
        if (searchButton.isEnabled()) {
            searchButton.click();
        } else {
            searchField.submit();
        }
    }
}
