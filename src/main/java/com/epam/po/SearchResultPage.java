package com.epam.po;

import com.epam.utils.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class SearchResultPage {
    private String title;
    private WebElement imageButton;

    public SearchResultPage() {
        title = WebDriverManager.getDriver().getTitle();
        imageButton = WebDriverManager.getDriver().findElement(By.className("qs"));
    }

    public void clickImageButton() {
        imageButton.click();
    }

    public String getTitle() {
        return title;
    }
}
