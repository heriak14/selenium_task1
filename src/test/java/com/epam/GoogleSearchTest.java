package com.epam;

import com.epam.bo.SearchBO;
import com.epam.utils.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GoogleSearchTest {
    private static Logger logger = LogManager.getLogger();
    private static final String BASE_PAGE = "https://google.com";
    private WebDriver driver;
    private SearchBO searchBO;

    @BeforeClass
    private void initBasePage() {
        logger.info("initializing WebDriver\n");
        driver = WebDriverManager.getDriver();
        assertNotNull(driver);
        logger.info("loading start page");
        driver.get(BASE_PAGE);
        searchBO = new SearchBO();
    }

    @Test
    private void testGoogleSearch() throws IOException {
        logger.info("test search for Apple\n");
        searchBO.search("Apple");
        (new WebDriverWait(driver, 20)).until(dr -> dr.getTitle().startsWith("Apple"));
        logger.info("Your page is: " + driver.getTitle() + "\n");
        File file = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file, new File("src/main/resources/google_screen.png"));
    }

    @Test
    private void testSwitchToImages() {
        logger.info("test switch to Images tab\n");
        searchBO.switchToImgTab();
        WebElement img = driver.findElement(By.xpath("//*[@id='hdtb-msb-vis']/div[@aria-selected='true']"));
        assertNotNull(img);
        assertEquals(img.getText(), "Зображення");
    }

    @AfterClass
    private void closePage() {
        logger.info("closing driver");
        WebDriverManager.closeDriver();
        driver.quit();
    }
}
